﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using FWMoviesAPI.Models;
using FWMoviesAPI.Dtos;

namespace FWMoviesAPI.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Movie, MovieDto>()
                .ForMember(dest => dest.YearOfRelease, opts => opts.MapFrom(source => source.ReleaseYear))
                .ForMember(dest => dest.RunningTime, opts => opts.MapFrom(source => source.RunningTimeInMiutes))
                .ForMember(dest => dest.AverageRating, opts => opts.MapFrom(source => GetAverageRatingForMovie(source)));

            CreateMap<RateMovieDto, UserMovieRating>();
        }

        private decimal GetAverageRatingForMovie(Movie movie)
        {
            if (movie.UserRatings.Count > 0)
            {
                decimal sum = 0;
                foreach (var userRating in movie.UserRatings)
                {
                    sum += userRating.Rating;
                }

                var averageRating = sum / movie.UserRatings.Count;
                return Math.Round(averageRating * 2, MidpointRounding.AwayFromZero) / 2;
            }
            return 0;
        }
    }
}
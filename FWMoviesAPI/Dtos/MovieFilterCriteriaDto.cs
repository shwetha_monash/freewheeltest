﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FWMoviesAPI.Dtos
{
    public class MovieFilterCriteriaDto
    {
        public string Title { get; set; }
        public int? YearOfRelease { get; set; }
        public List<int> GenreIds { get; set; }

        public MovieFilterCriteriaDto()
        {
            this.GenreIds = new List<int>();
        }
    }
}
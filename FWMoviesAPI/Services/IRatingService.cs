﻿using FWMoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FWMoviesAPI.Services
{
    public interface IRatingService
    {
        void AddRating(UserMovieRating rating);        
    }
}

﻿using FWMoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FWMoviesAPI.Services
{
    public class RatingService : IRatingService
    {
        private MovieModel _context;

        public RatingService()
        {
            _context = new MovieModel();
        }

        /// <summary>
        /// Adds or updates the rating for a user and movie
        /// </summary>
        /// <param name="userMovieRating"></param>
        public void AddRating(UserMovieRating userMovieRating)
        {
            // Validate the user, movie
            var user = _context.Users.SingleOrDefault(u => u.Id == userMovieRating.UserId);

            if (user == null)
                throw new Exception("User ID is invalid.");

            var movie = _context.Movies.SingleOrDefault(m => m.Id == userMovieRating.MovieId);

            if (movie == null)
                throw new Exception("Movie ID is invalid.");

            if (userMovieRating.Rating < 1 || userMovieRating.Rating > 5)
                throw new Exception("Movie rating needs to be between 1 and 5.");

            // Now, that validation is complete, check if this user has previously rated this movie, 
            // If so, update the rating with the current score
            // If not, create a new row in the UserMovieRatings table
            var previousRatingForMovieByUser = _context.UserMovieRatings
                .SingleOrDefault(umr => umr.MovieId == userMovieRating.MovieId && umr.UserId == userMovieRating.UserId);

            if (previousRatingForMovieByUser != null)
                previousRatingForMovieByUser.Rating = userMovieRating.Rating;
            else
                _context.UserMovieRatings.Add(userMovieRating);

            _context.SaveChanges();
        }        
    }
}
﻿using FWMoviesAPI.Dtos;
using FWMoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FWMoviesAPI.Services
{
    public interface IMovieService
    {
        IEnumerable<Movie> List(string title, int? yearOfRelease, List<int> genreIds);
        IEnumerable<MovieDto> TopRatedMovies(int count);
        IEnumerable<MovieDto> TopRatedMoviesByUser(int userId, int count);
    }
}

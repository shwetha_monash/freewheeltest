﻿using AutoMapper;
using FWMoviesAPI.Dtos;
using FWMoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace FWMoviesAPI.Services
{
    public class MovieService : IMovieService
    {
        private MovieModel _context;

        public MovieService()
        {
            _context = new MovieModel();
        }


        /// <summary>
        /// List movies based on criteria (title, year of release and genres)
        /// If no criteria are specified, returns 400 error
        /// If no records matching the criteria are found, it returns a 404 error
        /// </summary>
        /// <param name="title"></param>
        /// <param name="yearOfRelease"></param>
        /// <param name="genreIds"></param>
        /// <returns></returns>
        public IEnumerable<Movie> List(string title, int? yearOfRelease, List<int> genreIds)
        {
            // Throw exception if no filter criteria are specified
            if (string.IsNullOrWhiteSpace(title) && !yearOfRelease.HasValue && genreIds.Count == 0)
                throw new Exception("No filter criteria has been provided.");

            var movies = _context.Movies.Include(m => m.UserRatings).AsQueryable();

            if (!string.IsNullOrWhiteSpace(title))
                movies = movies.Where(m => m.Title.ToLower().Contains(title.ToLower()));

            if (yearOfRelease.HasValue)
                movies = movies.Where(m => m.ReleaseYear == yearOfRelease.Value);

            if (genreIds.Count > 0)
                movies = movies.Where(m => genreIds.Contains(m.GenreId));

            return movies.ToList();
        }

        /// <summary>
        /// Returns the top rated movies (average rating is rounded to nearest 0.5) 
        /// If more than 2 or more movies share the same average rating, they are alphabetically sorted within the group
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<MovieDto> TopRatedMovies(int count)
        {
            var moviesSortedByAverageRating = _context.Movies
                .Include(m => m.UserRatings)
                .Where(m => m.UserRatings.Count > 0)
                .Select(Mapper.Map<Movie, MovieDto>)
                .OrderByDescending(m => m.AverageRating)
                .ToList();

            var movieGroups = moviesSortedByAverageRating.GroupBy(m => m.AverageRating);

            List<MovieDto> topRatedMovies = new List<MovieDto>();
            foreach (var group in movieGroups)
            {
                foreach(var movie in group.OrderBy(h => h.Title))
                {
                    topRatedMovies.Add(movie);
                }
            }            

            return topRatedMovies.Take(count).AsEnumerable();
        }

        /// <summary>
        /// Returns the top rated movies for a particular user 
        /// If more than 2 or more movies share the same average rating, they are alphabetically sorted within the group
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<MovieDto> TopRatedMoviesByUser(int userId, int count)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == userId);

            if (user == null)
                throw new Exception("User ID is invalid.");

            var moviesRatedByUserAndSortedByRating = _context.UserMovieRatings.Where(ur => ur.UserId == userId).Include(ur => ur.Movie).OrderByDescending(ur => ur.Rating).ToList();

            var groups = moviesRatedByUserAndSortedByRating.GroupBy(ur => ur.Rating);

            List<MovieDto> topRatedMoviesByUser = new List<MovieDto>();
            foreach (var group in groups)
            {
                foreach (var ratingRow in group.OrderBy(h => h.Movie.Title))
                {
                    topRatedMoviesByUser.Add(Mapper.Map<Movie, MovieDto>(ratingRow.Movie));
                }
            }

            return topRatedMoviesByUser.Take(count).AsEnumerable();
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FWMoviesAPI.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public ICollection<UserMovieRating> MovieRatings { get; set; }
    }
}
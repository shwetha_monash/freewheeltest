﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FWMoviesAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public int ReleaseYear { get; set; }

        public int RunningTimeInMiutes { get; set; }

        public Genre Genre { get; set; }

        public int GenreId { get; set; }

        public ICollection<UserMovieRating> UserRatings { get; set; }
    }
}
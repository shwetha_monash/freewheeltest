﻿using AutoMapper;
using FWMoviesAPI.Dtos;
using FWMoviesAPI.Models;
using FWMoviesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FWMoviesAPI.Controllers
{
    public class RatingsController : ApiController
    {
        private IRatingService _ratingService;

        public RatingsController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        /// <summary>
        /// Post or submit a rating for a movie 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult PostRating([FromBody] RateMovieDto dto)
        {
            try
            {
                var userMovieRating = Mapper.Map<RateMovieDto, UserMovieRating>(dto);
                _ratingService.AddRating(userMovieRating);
                return Ok();
            }
            catch (Exception ex) when (ex.Message.Contains("ID is invalid"))
            {
                return NotFound();                
            }
            catch (Exception ex) when (ex.Message == "Movie rating needs to be between 1 and 5")
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

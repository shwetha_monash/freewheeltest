﻿using AutoMapper;
using FWMoviesAPI.Dtos;
using FWMoviesAPI.Models;
using FWMoviesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FWMoviesAPI.Controllers
{
    public class MoviesController : ApiController
    {
        private IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Return list of movies that match the spcified filter criteria
        /// </summary>
        /// <param name="filterCriteria"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetMovies([FromUri] MovieFilterCriteriaDto filterCriteria)
        {
            try
            {
                var movies = _movieService.List(filterCriteria.Title, filterCriteria.YearOfRelease, filterCriteria.GenreIds);

                if (movies.Count() == 0)
                    return NotFound();

                return Ok(movies.Select(Mapper.Map<Movie, MovieDto>));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Returns the top rated movies (average rating is rounded to nearest 0.5) 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetTopRatedMovies(int count = 5)
        {
            var topMovies = _movieService.TopRatedMovies(count);
            
            if (topMovies.Count() == 0)
                return NotFound();

            return Ok(topMovies);
        }


        /// <summary>
        /// Returns the top rated movies for a particular user 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetTopRatedMoviesByUser(int userId, int count = 5)
        {
            try
            {
                var topMovies = _movieService.TopRatedMoviesByUser(userId, count);

                if (topMovies.Count() == 0)
                    return NotFound();

                return Ok(topMovies);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

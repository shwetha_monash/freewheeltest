namespace FWMoviesAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRatingColumnToUserMovieRatingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserMovieRatings", "Rating", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserMovieRatings", "Rating");
        }
    }
}

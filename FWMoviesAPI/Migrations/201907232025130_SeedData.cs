namespace FWMoviesAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedData : DbMigration
    {
        public override void Up()
        {
            // Populate records in Genres table
            Sql("INSERT INTO Genres VALUES('Romance')");
            Sql("INSERT INTO Genres VALUES('Drama')");
            Sql("INSERT INTO Genres VALUES('Thriller')");
            Sql("INSERT INTO Genres VALUES('Action')");
            Sql("INSERT INTO Genres VALUES('Fantasy')");

            // Populate records in Users table
            Sql("INSERT INTO Users VALUES('John', 'Doe')");
            Sql("INSERT INTO Users VALUES('Jane', 'Doe')");
            Sql("INSERT INTO Users VALUES('Harry', 'Smith')");
            Sql("INSERT INTO Users VALUES('Mary', 'Thomas')");


            // Populate records in Movies table
            Sql("INSERT INTO Movies VALUES('Titanic', 1998, 120, 1)");
            Sql("INSERT INTO Movies VALUES('The Shawshank Redemption', 1995, 120, 3)");
            Sql("INSERT INTO Movies VALUES('The Green Mile', 2000, 180, 5)");
            Sql("INSERT INTO Movies VALUES('Driving Miss Daisy', 1990, 120, 2)");
            Sql("INSERT INTO Movies VALUES('Die Hard', 1989, 120, 4)");
        }

        public override void Down()
        {
        }
    }
}

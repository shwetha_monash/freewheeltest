﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using SimpleInjector;
using FWMoviesAPI.Services;
using SimpleInjector.Integration.WebApi;
using AutoMapper;
using FWMoviesAPI.App_Start;

namespace FWMoviesAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());

            var container = new Container();
            container.Register<IRatingService, RatingService>();
            container.Register<IMovieService, MovieService>();

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
